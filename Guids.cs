﻿// Guids.cs
// MUST match guids.h
using System;

namespace Dealdiane.QuickWebDebug
{
    static class GuidList
    {
        public const string guidQuickWebDebugPkgString = "967dc17c-9dec-4082-b9b9-bc9ecd887aac";
        public const string guidQuickWebDebugCmdSetString = "99fe208a-2cb8-4e21-b693-be6871257466";

        public static readonly Guid guidQuickWebDebugCmdSet = new Guid(guidQuickWebDebugCmdSetString);
    };
}