﻿// PkgCmdID.cs
// MUST match PkgCmdID.h
using System;

namespace Dealdiane.QuickWebDebug
{
    static class PkgCmdIDList
    {
        //public const uint cmdidGenLipsum         = 0x0100;
        //public const uint cmdidBlockComment      = 0x0101;
        public const uint cmdidAttachWebServer   = 0x0100;
        //public const uint cmdidMRUList           = 0x0103;
        //public const uint cmdidGenLipsumPreset1  = 0x0104;
        public const uint cmdidReattachWebServer = 0x0101;
        //public const uint cmdidGenLipsumPreset2  = 0x0105;
        //public const uint cmdidGenLipsumPreset3  = 0x0106;
        //public const uint cmdidGenLipsumPreset4  = 0x0107;
        //public const uint cmdidGenLipsumPreset5  = 0x0108;
        //public const uint cmdidGenLipsumPreset6  = 0x0109;
        //public const uint cmdidGenLipsumPreset7  = 0x010a;
        //public const uint cmdidGenLipsumPreset8  = 0x010b;
        //public const uint cmdidGenLipsumPreset9  = 0x010c;
        //public const uint cmdidGenLipsumPreset10 = 0x010d;
        //public const uint cmdidGenRandomPeson    = 0x0110;
    };
}