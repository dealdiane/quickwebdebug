﻿using Dealdiane.QuickWebDebug.Properties;
using Microsoft.Web.Administration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

namespace Dealdiane.QuickWebDebug
{
    public partial class SelectProcessForm : Form
    {
        private readonly IEnumerable<EnvDTE.Process> _processes;

        public SelectProcessForm(IEnumerable<EnvDTE.Process> processes)
        {
            _processes = processes;

            InitializeComponent();

            ConfigureFormSize();
        }

        public int SelectedProcessID
        {
            get
            {
                if (processComboBox == null || processComboBox.SelectedItem == null)
                {
                    return 0;
                }

                return ((ServerProcess)processComboBox.SelectedItem).Process.Id;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            var processes = GetProcesses().Select(p => new ServerProcess(p)).ToList();
            var iisProcesses = Process.GetProcessesByName("w3wp");

            if (iisProcesses.Length > 0)
            {
                var iisManager = new ServerManager();

                using (iisManager)
                {
                    foreach (var iisProcess in iisProcesses)
                    {
                        var appDomains = iisManager.WorkerProcesses.Single(p => p.ProcessId == iisProcess.Id).ApplicationDomains;

                        if (appDomains.Count > 0)
                        {
                            processes.AddRange(appDomains.Select(a => new ServerProcess(a)));
                        }
                        else
                        {
                            processes.Add(new ServerProcess(iisProcess));
                        }

                        //.Select(a => new KeyValuePair<int, string>(a.WorkerProcess.ProcessId, string.Format("[{0}][{1}] {2}.exe", iisProcess.Id, a.Id, iisProcess.ProcessName))));
                    }
                }
            }

            if (processes.Count == 0)
            {
                MessageBox.Show(this, "Could not find any running web process.", "Web Debug", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();

                return;
            }

            processComboBox.DataSource = processes.ToArray();
            processComboBox.DisplayMember = "Title";

            base.OnLoad(e);
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            switch (m.Msg)
            {
                case 0x84: //WM_NCHITTEST
                    var result = (HitTest)m.Result.ToInt32();

                    if (result == HitTest.Top || result == HitTest.Bottom)
                    {
                        m.Result = new IntPtr((int)HitTest.Caption);
                    }

                    if (result == HitTest.TopLeft || result == HitTest.BottomLeft)
                    {
                        m.Result = new IntPtr((int)HitTest.Left);
                    }

                    if (result == HitTest.TopRight || result == HitTest.BottomRight)
                    {
                        m.Result = new IntPtr((int)HitTest.Right);
                    }

                    break;
            }
        }

        private static string GetSiteNameFromIISExpress(Process process)
        {
            var commandLine = ProcessUtilities.GetCommandLine(process, @"/site:""(?<Name>.*?)""");

            if (commandLine == null)
            {
                return string.Empty;
            }

            return commandLine.Groups["Name"].Value;
        }

        private void AttachButton_Click(object sender, EventArgs e)
        {
            if (processComboBox.SelectedItem == null)
            {
                MessageBox.Show(this, "Select a process to attach to", "QuickWebDebug", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        private void ConfigureFormSize()
        {
            if (Settings.Default.LastSelectProcessFormWidth > 0)
            {
                Size = new System.Drawing.Size(Settings.Default.LastSelectProcessFormWidth, Size.Height);
            }

            if (MaximumSize.IsEmpty)
            {
                return;
            }

            int screenWidth;

            try
            {
                Screen currentScreen;

                try
                {
                    currentScreen = Screen.FromControl(this);
                }
                catch
                {
                    currentScreen = Screen.PrimaryScreen;
                }

                if (currentScreen == null)
                {
                    screenWidth = 0;
                }
                else
                {
                    screenWidth = currentScreen.Bounds.Width;
                }
            }
            catch
            {
                screenWidth = 0;
            }

            if (screenWidth > 0 && screenWidth > MaximumSize.Width)
            {
                MaximumSize = new System.Drawing.Size(screenWidth, MaximumSize.Height);
            }
        }

        private IEnumerable<Process> GetProcesses()
        {
            foreach (var process in _processes)
            {
                yield return Process.GetProcessById(process.ProcessID);
            }

            //foreach (var processName in ServerProcessNames.Where(p => !p.Equals("w3wp", StringComparison.OrdinalIgnoreCase)))
            //{
            //    foreach (var process in Process.GetProcessesByName(processName))
            //    {
            //        yield return process;
            //    }
            //}
        }

        private WebDevSiteInfo GetSiteInforFromWebDevProcess(Process process)
        {
            var match = ProcessUtilities.GetCommandLine(process, @"/(?<Key>\w+):""?(?<Value>.+?)(?:""|\s)");
            var siteInfo = new WebDevSiteInfo();

            while (match.Success)
            {
                var groups = match.Groups;
                var key = groups["Key"].Value;

                switch (key.ToUpperInvariant())
                {
                    case "PORT":
                        siteInfo.Port = Convert.ToInt32(groups["Value"].Value);
                        break;

                    case "PATH":
                        siteInfo.Root = groups["Value"].Value;
                        break;

                    case "VPATH":
                        siteInfo.VirtualPath = groups["Value"].Value;
                        break;

                    default:
                        break;
                }

                match = match.NextMatch();
            }

            return siteInfo;
        }

        private void ProcessComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var process = processComboBox.SelectedItem as ServerProcess;

            if (process == null)
            {
                commonToolTip.Active = false;
                return;
            }

            var message = $@"
Process: {process.Process.ProcessName}
ID: {process.Process.Id}
User: {ProcessUtilities.GetProcessOwner(process.Process)}";

            if (process.AppDomain != null)
            {
                var appDomain = process.AppDomain;

                message += $@"
AppPool: {appDomain.WorkerProcess.AppPoolName}
Path: {appDomain.PhysicalPath}
Virtual Path: {appDomain.VirtualPath}
";
            }
            else if (string.Equals(process.Process.ProcessName, "iisexpress"))
            {
                var site = GetSiteNameFromIISExpress(process.Process);

                if (!string.IsNullOrEmpty(site))
                {
                    message += $@"
Site: {site}
";
                }
            }
            else if (process.Process.ProcessName.StartsWith("webdev", StringComparison.OrdinalIgnoreCase))
            {
                var siteInfo = GetSiteInforFromWebDevProcess(process.Process);
                message += $@"
Site: localhost:{siteInfo.Port}{siteInfo.VirtualPath}
Root: {siteInfo.Root}
";
            }
            else if (process.Process.ProcessName.StartsWith("dnx", StringComparison.OrdinalIgnoreCase)
                || process.Process.ProcessName.StartsWith("dotnet", StringComparison.OrdinalIgnoreCase))
            {
                var commandLine = ProcessUtilities.GetCommandLine(process.Process, ".*");

                if (commandLine?.Success == true)
                {
                    message += $@"
Command Line: {commandLine.Value}";
                }

                var workingDirectory = process.Process.StartInfo?.WorkingDirectory;

                if (String.IsNullOrWhiteSpace(workingDirectory))
                {
                    try
                    {
                        workingDirectory = ProcessUtilities.GetCurrentDirectory(process.Process.Id);
                    }
                    catch (Win32Exception)
                    {
                    }
                }

                if (!String.IsNullOrWhiteSpace(workingDirectory))
                {
                    message += $@"
Working Directory: {workingDirectory}";
                }
            }

            commonToolTip.SetToolTip(processComboBox, message?.Trim());
            commonToolTip.Active = true;
        }

        private void SelectProcessForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                processComboBox.SelectedItem = null;
                e.Handled = true;
                DialogResult = System.Windows.Forms.DialogResult.Cancel;
                Close();
            }
        }

        private void SelectProcessForm_ResizeEnd(object sender, EventArgs e)
        {
            Settings.Default.LastSelectProcessFormWidth = Size.Width;
            Settings.Default.Save();
        }

        private struct WebDevSiteInfo
        {
            public int Port { get; set; }

            public string Root { get; set; }

            public string VirtualPath { get; set; }
        }
    }
}