﻿using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Dealdiane.QuickWebDebug
{
    /// <summary>
    /// This is the class that implements the package exposed by this assembly.
    ///
    /// The minimum requirement for a class to be considered a valid package for Visual Studio
    /// is to implement the IVsPackage interface and register itself with the shell.
    /// This package uses the helper classes defined inside the Managed Package Framework (MPF)
    /// to do it: it derives from the Package class that provides the implementation of the
    /// IVsPackage interface and uses the registration attributes defined in the framework to
    /// register itself and its components with the shell.
    /// </summary>
    // This attribute tells the PkgDef creation utility (CreatePkgDef.exe) that this class is
    // a package.
    [PackageRegistration(UseManagedResourcesOnly = true)]
    // This attribute is used to register the information needed to show this package
    // in the Help/About dialog of Visual Studio.
    [InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 400)]
    // This attribute is needed to let the shell know that this package exposes some menus.
    [ProvideMenuResource("Menus.ctmenu", 1)]
    [Guid(GuidList.guidQuickWebDebugPkgString)]
    [ProvideOptionPage(typeof(Dealdiane.QuickWebDebug.Configuration.GeneralConfiguration), "Quick Web Debug", "General", 0, 0, true)]
    [ProvideAutoLoad(VSConstants.UICONTEXT.NoSolution_string)]
    public sealed class QuickWebDebugPackage : Package, IWin32Window
    {
        private const int _presetCount = 10;
        private static readonly Guid _outputPaneId = new Guid("6EA21955-1971-4E7C-9468-8ED2F557575C");
        private static DebuggerEvents _debuggerEvents;
        private static int _lastAttachedProcessID = 0;
        private DTE2 _applicationObject;

        /// <summary>
        /// Default constructor of the package.
        /// Inside this method you can place any initialization code that does not require
        /// any Visual Studio service because at this point the package object is created but
        /// not sited yet inside Visual Studio environment. The place to do all the other
        /// initialization is the Initialize method.
        /// </summary>
        public QuickWebDebugPackage()
        {
            Debug.WriteLine(string.Format(CultureInfo.CurrentCulture, "Entering constructor for: {0}", this.ToString()));
        }

        public IntPtr Handle
        {
            get
            {
                return new System.IntPtr(_applicationObject.MainWindow.HWnd);
            }
        }

        private static Assembly AssemblyResolve_EventHandler(object sender, System.ResolveEventArgs e)
        {
            var asm = AppDomain.CurrentDomain.GetAssemblies();

            for (int i = 0; i < asm.Length; i++)
            {
                if (asm[i].FullName == e.Name)
                {
                    return asm[i];
                }
            }

            Debug.Print("Could not find assembly: " + e.Name);
            //System.Windows.Forms.MessageBox.Show("Could not find assembly: " + e.Name);0

            return null;
        }

        [DllImport("user32.dll")]
        private static extern short GetKeyState(int nVirtKey);

        /////////////////////////////////////////////////////////////////////////////
        // Overridden Package Implementation

        #region Package Members

        /// <summary>
        /// Initialization of the package; this method is called right after the package is sited, so this is the place
        /// where you can put all the initialization code that rely on services provided by VisualStudio.
        /// </summary>
        protected override void Initialize()
        {
            Debug.WriteLine(string.Format(CultureInfo.CurrentCulture, "Entering Initialize() of: {0}", this.ToString()));

            _applicationObject = Package.GetGlobalService(typeof(DTE)) as DTE2;

            base.Initialize();

            // Add our command handlers for menu (commands must exist in the .vsct file)
            OleMenuCommandService mcs = GetService(typeof(IMenuCommandService)) as OleMenuCommandService;

            if (null != mcs)
            {
                // Create the command for the menu item.
                var menuCommandID1 = new CommandID(GuidList.guidQuickWebDebugCmdSet, (int)PkgCmdIDList.cmdidAttachWebServer);
                var menuItem1 = new MenuCommand(AttachToWebServer, menuCommandID1);

                mcs.AddCommand(menuItem1);

                var menuCommandID2 = new CommandID(GuidList.guidQuickWebDebugCmdSet, (int)PkgCmdIDList.cmdidReattachWebServer);
                var menuItem2 = new MenuCommand(ReattachToWebServer, menuCommandID2);

                mcs.AddCommand(menuItem2);
            }

            if (_debuggerEvents == null)
            {
                _debuggerEvents = _applicationObject.Events.DebuggerEvents;
                _debuggerEvents.OnEnterRunMode += DebuggerEvents_OnEnterRunMode;
            }

            AppDomain.CurrentDomain.AssemblyResolve += AssemblyResolve_EventHandler;
        }

        #endregion Package Members

        private void AttachToProcess()
        {
            var _applicationObject = Package.GetGlobalService(typeof(DTE)) as DTE2;
            var localProcesses = _applicationObject.Debugger.LocalProcesses.OfType<EnvDTE.Process>();

            var webProcesses = localProcesses
                .Where(process => WebProcessHelper.WebProcessNames.Any(name => process.Name.EndsWith(name + ".exe", StringComparison.OrdinalIgnoreCase)))
                .ToList();

            EnvDTE.Process selectedProcess = null;

            if (_lastAttachedProcessID > 0)
            {
                WriteMessage($"Re-attaching to last attached process with id: {_lastAttachedProcessID}.");
                selectedProcess = webProcesses.SingleOrDefault(p => p.ProcessID == _lastAttachedProcessID);

                if (selectedProcess == null)
                {
                    WriteMessage($"Process not found or no longer active");
                }

                /*if (selectedProcess == null)
                {
                    // reset if can't find last attached process
                    _lastProcessID = -1;
                }*/
            }

            if (selectedProcess == null)
            {
                if (webProcesses.Count == 0)
                {
                    var outputProcess = WebProcessHelper.GetStartupProjectOutputProcess(_applicationObject.Solution, localProcesses);

                    if (outputProcess != null)
                    {
                        webProcesses.Add(outputProcess);
                    }
                }

                if (webProcesses.Count > 1 /*&& _lastProcessID == -1*/)
                {
                    selectedProcess = WebProcessHelper.GetSolutionProcess(_applicationObject.Solution, webProcesses, localProcesses);

                    if (selectedProcess == null)
                    {
                        var debugProcessList = webProcesses.ToList();

                        foreach (var webProcess in webProcesses)
                        {
                            debugProcessList.AddRange(
                                ProcessUtilities.GetChildProcessIds(webProcess.ProcessID)
                                    .Where(processId => !debugProcessList.Any(debugProcess => debugProcess.ProcessID == processId))
                                    .Select(processId => localProcesses.SingleOrDefault(localProcess => localProcess.ProcessID == processId))
                                    .Where(process => process != null));
                        }

                        using (var form = new SelectProcessForm(debugProcessList))
                        {
                            var result = form.ShowDialog(this);

                            if (result != DialogResult.Cancel)
                            {
                                selectedProcess = webProcesses.FirstOrDefault(p => p.ProcessID == form.SelectedProcessID);

                                if (selectedProcess == null && form.SelectedProcessID > 0)
                                {
                                    MessageBox.Show(this, "The process could not be found. Ensure that the user has access and that the process is still running.", "Quick Web Debug", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                        }
                    }
                }
                else if (webProcesses.Count == 1)
                {
                    selectedProcess = webProcesses[0];
                }
                else
                {
                    MessageBox.Show(this, "Cannot find any running web server process. Ensure that at least one web server is running.", "Quick Web Debug", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            if (selectedProcess != null)
            {
                _lastAttachedProcessID = selectedProcess.ProcessID;
                selectedProcess.Attach();

                WriteMessage($"Attached to process: {selectedProcess.Name}({_lastAttachedProcessID})");
            }
        }

        private void AttachToWebServer(object sender, EventArgs e)
        {
            // Clears the last attached process to force the form to display
            _lastAttachedProcessID = 0;

            AttachToProcess();
        }

        private void BlockcommentCallBack(object sender, EventArgs e)
        {
            if (_applicationObject.ActiveDocument != null)
            {
                var selection = _applicationObject.ActiveDocument.Selection as TextSelection;

                if (selection != null && !string.Equals(selection.Text, string.Empty))
                {
                    var text = selection.Text;

                    _applicationObject.UndoContext.Open("CStyleComment");
                    selection.Delete();

                    if (Regex.IsMatch(text, @"\s*/\*[\w\W]*\*/\s*", RegexOptions.Multiline | RegexOptions.CultureInvariant))
                    {
                        selection.Insert(Regex.Replace(text, @"(\s*)/\*([\w\W]*)\*/(\s*)", "$1$2$3"));
                    }
                    else
                    {
                        selection.Insert("/*" + text + "*/");
                    }

                    _applicationObject.UndoContext.Close();
                }
            }
        }

        private void DebuggerEvents_OnEnterRunMode(dbgEventReason reason)
        {
            if (reason == dbgEventReason.dbgEventReasonAttachProgram || reason == dbgEventReason.dbgEventReasonGo || reason == dbgEventReason.dbgEventReasonLaunchProgram)
            {
                var processes = _applicationObject.DTE.Debugger.DebuggedProcesses;
                if (processes.Count > 0)
                {
                    _lastAttachedProcessID = processes.Item(1).ProcessID;
                }
            }
        }

        /// <summary>
        /// This function is the callback used to execute a command when the a menu item is clicked.
        /// See the Initialize method to see how the menu item is associated to this function using
        /// the OleMenuCommandService service and the MenuCommand class.
        /// </summary>
        private void MenuItemCallback(object sender, EventArgs e)
        {
            // Show a Message Box to prove we were here
            IVsUIShell uiShell = (IVsUIShell)GetService(typeof(SVsUIShell));
            Guid clsid = Guid.Empty;
            int result;
            Microsoft.VisualStudio.ErrorHandler.ThrowOnFailure(uiShell.ShowMessageBox(
                       0,
                       ref clsid,
                       "Quick Web Debug",
                       string.Format(CultureInfo.CurrentCulture, "Inside {0}.MenuItemCallback()", this.ToString()),
                       string.Empty,
                       0,
                       OLEMSGBUTTON.OLEMSGBUTTON_OK,
                       OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST,
                       OLEMSGICON.OLEMSGICON_INFO,
                       0,        // false
                       out result));
        }

        private void ReattachToWebServer(object sender, EventArgs e)
        {
            AttachToProcess();
        }

        private void WriteMessage(string message)
        {
            // http://stackoverflow.com/a/1852535/1461061

            var outWindow = Package.GetGlobalService(typeof(SVsOutputWindow)) as IVsOutputWindow;

            // Use e.g. Tools -> Create GUID to make a stable, but unique GUID for your pane.
            // Also, in a real project, this should probably be a static constant, and not a local variable
            var paneGuid = _outputPaneId;
            var customTitle = "Quick Web Debug";
            outWindow.CreatePane(ref paneGuid, customTitle, 1, 1);

            IVsOutputWindowPane customPane;

            if (outWindow.GetPane(ref paneGuid, out customPane) != Microsoft.VisualStudio.VSConstants.S_OK)
            {
                return;
            }

            customPane.OutputString(message + Environment.NewLine);
            customPane.Activate(); // Brings this pane into view
        }
    }
}