﻿using Microsoft.VisualStudio.Shell;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace Dealdiane.QuickWebDebug.Configuration
{
    //[ComVisible(true)]
    //public enum Gender
    //{
    //    Both,
    //    Male,
    //    Female
    //}

    [ClassInterface(ClassInterfaceType.AutoDual)]
    [CLSCompliant(false), ComVisible(true)]
    public class GeneralConfiguration : DialogPage
    {
        //[Category("Lipsum Generator")]
        //[DisplayName("Lipsum Configurations")]
        //[TypeConverter(typeof(LipsumConfigurationArrayConverter))]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public LipsumConfiguration[] LipsumConfigurations
        //{
        //    get;
        //    set;
        //}

        [DisplayName("Auto attach to preselected process")]
        //[Category("Web Debug")]
        [DefaultValue(true)]
        public bool ShouldAttachToPreselectedProcess { get; set; } = true;

        public override void LoadSettingsFromStorage()
        {
            base.LoadSettingsFromStorage();
        }

        protected override void OnApply(DialogPage.PageApplyEventArgs e)
        {
            base.OnApply(e);
        }
    }
}