﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text.RegularExpressions;

namespace Dealdiane.QuickWebDebug
{
    public static partial class ProcessUtilities
    {
        private static uint TOKEN_QUERY = 0x0008;

        public static IEnumerable<int> GetChildProcessIds(System.Diagnostics.Process parentProcess)
        {
            return GetChildProcessIds(parentProcess.Id);
        }

        public static IEnumerable<int> GetChildProcessIds(int processId)
        {
            var searcher = new ManagementObjectSearcher($"SELECT * FROM Win32_Process WHERE ParentProcessId = {processId}");

            foreach (var process in searcher.Get())
            {
                yield return Convert.ToInt32(process["ProcessId"]);
            }
        }

        public static Match GetCommandLine(System.Diagnostics.Process process, string pattern)
        {
            using (var searcher = new ManagementObjectSearcher("SELECT CommandLine FROm Win32_Process WHERE ProcessId=" + process.Id))
            {
                foreach (var mgmtObject in searcher.Get())
                {
                    var commandLine = Convert.ToString(mgmtObject["CommandLine"]);

                    if (String.IsNullOrEmpty(commandLine))
                    {
                        continue;
                    }

                    var match = Regex.Match(commandLine, pattern, RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

                    if (match.Success)
                    {
                        return match;
                    }
                }
            }

            return null;
        }

        public static string GetExecutablePath(System.Diagnostics.Process process)
        {
            try
            {
                return process.MainModule.FileName;
            }
            catch (Win32Exception)
            {
                // 32-bit process trying to read a 64-bit process info
            }

            using (var searcher = new ManagementObjectSearcher($"SELECT ExecutablePath FROM Win32_Process WHERE ProcessId = {process.Id}"))
            {
                return searcher.Get().Cast<ManagementBaseObject>().SingleOrDefault()?["ExecutablePath"] as string;
            }
        }

        public static string GetProcessOwner(Process process)
        {
            var ph = IntPtr.Zero;

            try
            {
                OpenProcessToken(process.Handle, TOKEN_QUERY, out ph);

                using (var wi = new WindowsIdentity(ph))
                {
                    return wi.Name;
                }
            }
            catch (Exception)
            {
                return "Error";
            }
            finally
            {
                if (ph != IntPtr.Zero)
                {
                    CloseHandle(ph);
                }
            }
        }

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool OpenProcessToken(IntPtr ProcessHandle, UInt32 DesiredAccess, out IntPtr TokenHandle);
    }
}