﻿using EnvDTE;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics.Extensions;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Dealdiane.QuickWebDebug
{
    public class WebProcessHelper
    {
        public static readonly string[] WebProcessNames =
            {
                "WebDev.WebServer20",
                "WebDev.WebServer40",
                "iisexpress",
                "w3wp",
                "dnx",
                "dotnet",
            };

        public static Process GetSolutionProcess(Solution solution, IEnumerable<Process> webProcesses, IEnumerable<Process> localProcesses)
        {
            foreach (var process in new[]
                {
                    GetStartupProjectOutputProcess(solution, localProcesses),
                    GetDotNetProcess(solution, webProcesses),
                    GetDotNetWatchProcess(solution, webProcesses, localProcesses),
                    GetDnxProcess(solution, webProcesses),
                })
            {
                if (process != null)
                {
                    return process;
                }
            }

            // TODO: Find IIS Process
            // TODO: Find IISExpress Process

            return null;
        }

        public static Process GetStartupProjectOutputProcess(Solution solution, IEnumerable<Process> localProcesses)
        {
            var startupProject = FindStartupProject(solution);

            if (startupProject == null)
            {
                return null;
            }

            var outputFilePath = GetOutputFilePath(startupProject) + ".exe";

            return localProcesses
                .Where(localProcess => String.Equals(localProcess.Name, outputFilePath, StringComparison.OrdinalIgnoreCase))
                .FirstOrDefault();
        }

        private static Process FindDotNetWatchChildProcess(IEnumerable<Process> webProcesses, IEnumerable<Process> localProcesses, Func<System.Diagnostics.Process, bool> matchCallback)
        {
            foreach (var item in webProcesses)
            {
                var process = System.Diagnostics.Process.GetProcessById(item.ProcessID);

                if (!process.IsDotNet())
                {
                    continue;
                }

                var match = ProcessUtilities.GetCommandLine(process, @"\s*""?dotnet(\.exe)?""?\s+run");

                if (match?.Success == true)
                {
                    foreach (var childProcessId in ProcessUtilities.GetChildProcessIds(process))
                    {
                        var childProcess = System.Diagnostics.Process.GetProcessById(childProcessId);
                        var isMatch = matchCallback(childProcess);

                        if (isMatch)
                        {
                            return localProcesses.Single(p => p.ProcessID == childProcessId);
                        }
                    }
                }
            }

            return null;
        }

        private static Project FindProjectByUniqueName(string uniqueName, Solution solution)
        {
            foreach (Project project in solution.Projects)
            {
                if (String.Equals(project.UniqueName, uniqueName, StringComparison.OrdinalIgnoreCase))
                {
                    return project;
                }

                var subProject = FindProjectByUniqueName(uniqueName, project.ProjectItems);

                if (subProject != null)
                {
                    return subProject;
                }
            }

            return null;
        }

        private static Project FindProjectByUniqueName(string uniqueName, ProjectItems items)
        {
            if (items == null)
            {
                return null;
            }

            foreach (ProjectItem item in items)
            {
                if (String.Equals(item.SubProject?.UniqueName, uniqueName, StringComparison.OrdinalIgnoreCase))
                {
                    return item.SubProject;
                }

                var project = FindProjectByUniqueName(uniqueName, item.ProjectItems);

                if (project != null)
                {
                    return project;
                }
            }

            return null;
        }

        private static string FindProjectJsonPath(Solution solution)
        {
            // ASP.NET 5 preselect
            var startupProject = FindStartupProject(solution);

            if (startupProject?.IsNetCoreProjectJson() == true)
            {
                try
                {
                    var projectJson = startupProject.ProjectItems.Item("project.json");

                    if (projectJson != null)
                    {
                        return Convert.ToString(projectJson.Properties.Item("FullPath")?.Value);
                    }
                }
                catch (ArgumentException)
                {
                    return null;
                }
            }

            return null;
        }

        private static Project FindStartupProject(Solution solution)
        {
            var startupProjects = (object[])solution.SolutionBuild.StartupProjects;

            // Solution with multiple startup projects is not supported (yet)
            if (startupProjects?.Length == 1)
            {
                var startupProjectUniqueName = Convert.ToString(startupProjects[0]);

                return FindProjectByUniqueName(startupProjectUniqueName, solution);
            }

            return null;
        }

        private static Process GetDnxProcess(Solution solution, IEnumerable<Process> processes)
        {
            var projectJsonPath = FindProjectJsonPath(solution);

            if (!String.IsNullOrWhiteSpace(projectJsonPath))
            {
                foreach (var item in processes)
                {
                    var process = System.Diagnostics.Process.GetProcessById(item.ProcessID);

                    if (!process.ProcessName.StartsWith("dnx", StringComparison.OrdinalIgnoreCase))
                    {
                        continue;
                    }

                    var match = ProcessUtilities.GetCommandLine(process, @"--project\s+(?<Path>(\"".+\""|.+?))\s+");

                    if (match?.Success == true && String.Equals(match.Groups["Path"]?.Value.Trim('"', ' '), projectJsonPath, StringComparison.OrdinalIgnoreCase))
                    {
                        return item;
                    }
                }
            }

            return null;
        }

        private static Process GetDotNetProcess(Solution solution, IEnumerable<Process> processes)
        {
            var projectJsonPath = FindProjectJsonPath(solution);

            string commandLinePattern = null;

            if (!String.IsNullOrWhiteSpace(projectJsonPath))
            {
                var projectDirectory = Path.GetDirectoryName(projectJsonPath);
                var dllFileName = Path.GetFileName(projectDirectory);

                commandLinePattern = $@"{projectDirectory.TrimEnd('\\').Replace(@"\", @"\\")}\\bin\\.+\\.+\\{dllFileName}.dll";
            }

            // TODO: Support dotnet run {filename}
            //var startupProject = FindStartupProject(solution);

            //if (startupProject?.IsNetCoreCsProj() == true)
            //{
            //    var asssemblyName = startupProject.Properties.Item("AssemblyName");
            //    var projectDirectory = Path.GetDirectoryName(startupProject.FileName);

            //    commandLinePattern = $@"{projectDirectory.TrimEnd('\\').Replace(@"\", @"\\")}\\bin\\.+\\.+\\(.+\\)?{asssemblyName.Value}.(dll|exe)";
            //}

            if (!String.IsNullOrWhiteSpace(commandLinePattern))
            {
                foreach (var item in processes)
                {
                    var process = System.Diagnostics.Process.GetProcessById(item.ProcessID);

                    if (!process.IsDotNet())
                    {
                        continue;
                    }

                    var match = ProcessUtilities.GetCommandLine(process, commandLinePattern);

                    if (match?.Success == true)
                    {
                        return item;
                    }
                }
            }

            return null;
        }

        private static Process GetDotNetWatchProcess(Solution solution, IEnumerable<Process> webProcesses, IEnumerable<Process> localProcesses)
        {
            var projectJsonPath = FindProjectJsonPath(solution);

            if (!String.IsNullOrWhiteSpace(projectJsonPath))
            {
                var exeFileName = GetExeFileName(projectJsonPath);
                var projectDirectory = Path.GetDirectoryName(projectJsonPath);
                var expectedProjectExecutablePath = $@"{projectDirectory.TrimEnd('\\').Replace(@"\", @"\\")}\\bin\\([^\\]+\\){{3}}{exeFileName}.exe";
                var matchedChildProcess = FindDotNetWatchChildProcess(
                    webProcesses,
                    localProcesses,
                    childProcess => Regex.IsMatch(
                        ProcessUtilities.GetExecutablePath(childProcess),
                        expectedProjectExecutablePath,
                        RegexOptions.CultureInvariant | RegexOptions.IgnoreCase));

                if (matchedChildProcess != null)
                {
                    return matchedChildProcess;
                }
            }

            var startupProject = FindStartupProject(solution);

            if (startupProject?.IsNetCoreCsProj() == true)
            {
                var outputFileName = GetOutputFilePath(startupProject);

                var matchedChildProcess = FindDotNetWatchChildProcess(
                    webProcesses,
                    localProcesses,
                    childProcess =>
                    {
                        var processFullPath = !Environment.Is64BitProcess && !childProcess.IsWin64Emulator()
                            ? childProcess.GetProcessPath()
                            : childProcess.MainModule.FileName;

                        return
                            String.Equals(processFullPath, outputFileName + ".exe", StringComparison.OrdinalIgnoreCase)
                            || String.Equals(processFullPath, outputFileName + ".dll", StringComparison.OrdinalIgnoreCase)
                            || ProcessUtilities.GetCommandLine(childProcess, $@"""?dotnet""?\s+exec\s+""?{outputFileName.Replace(@"\", @"\\")}(.exe|.dll)""?")?.Success == true;
                    });

                if (matchedChildProcess != null)
                {
                    return matchedChildProcess;
                }
            }

            return null;
        }

        private static string GetExeFileName(string projectJsonPath)
        {
            string exeFileName = null;

            if (File.Exists(projectJsonPath))
            {
                string projectJsonContents = null;

                try
                {
                    projectJsonContents = File.ReadAllText(projectJsonPath);
                }
                catch
                {
                    projectJsonContents = null;
                }

                if (!String.IsNullOrWhiteSpace(projectJsonContents))
                {
                    dynamic projectConfig;

                    try
                    {
                        projectConfig = JsonConvert.DeserializeObject(projectJsonContents);
                    }
                    catch (JsonReaderException)
                    {
                        projectConfig = null;
                    }

                    if (projectConfig != null)
                    {
                        exeFileName = Convert.ToString(projectConfig.buildOptions?.outputName);
                    }
                }
            }

            if (String.IsNullOrWhiteSpace(exeFileName))
            {
                var projectDirectory = Path.GetDirectoryName(projectJsonPath);

                exeFileName = Path.GetFileName(projectDirectory);
            }

            return exeFileName;
        }

        private static string GetOutputFilePath(Project project)
        {
            // TODO: Find a way to get the 'real' output filename
            var projectPath = project.Properties.Item("FullPath").Value.ToString();
            var asssemblyName = project.Properties.Item("AssemblyName").Value.ToString();
            var outputPath = project.ConfigurationManager.ActiveConfiguration.Properties.Item("OutputPath").Value.ToString();

            return Path.Combine(projectPath, Path.Combine(outputPath.TrimEnd('\\'), asssemblyName));
        }
    }
}