﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Management;
using System.Runtime.InteropServices;

namespace System.Diagnostics.Extensions
{
    public static class ProcessExtensions
    {
        public static string GetProcessPath(this Process process)
        {
            using (var searcher = new ManagementObjectSearcher($"SELECT ProcessId, ExecutablePath, CommandLine FROM Win32_Process WHERE ProcessId = {process.Id}"))
            using (var results = searcher.Get())
            {
                var wmiProcess = results
                    .Cast<ManagementBaseObject>()
                    .SingleOrDefault();

                return Convert.ToString(wmiProcess?["ExecutablePath"]);
            }
        }

        public static bool IsDotNet(this Process process)
                    => process.ProcessName.Equals("dotnet", StringComparison.OrdinalIgnoreCase);

        public static bool IsWin64Emulator(this Process process)
        {
            if ((Environment.OSVersion.Version.Major > 5)
                || ((Environment.OSVersion.Version.Major == 5) && (Environment.OSVersion.Version.Minor >= 1)))
            {
                return NativeMethods.IsWow64Process(process.Handle, out var retVal) && retVal;
            }

            return false; // Not on 64-bit Windows Emulator
        }

        internal static class NativeMethods
        {
            [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
            [return: MarshalAs(UnmanagedType.Bool)]
            internal static extern bool IsWow64Process([In] IntPtr process, [Out] out bool wow64Process);
        }
    }
}