﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace EnvDTE
{
    public static class ProjectExtensions
    {
        private static readonly Guid _dnxProjectGuid = Guid.Parse("8BB2217D-0F2D-49D1-97BC-3654ED321F3B");
        private static readonly Guid _dotNetProjectGuid = Guid.Parse("9A19103F-16F7-4668-BE54-9A1E7A4F7556");

        public static bool IsNetCoreCsProj(this Project project)
        {
            return Guid.TryParse(project?.Kind, out var projectGuid) && projectGuid == _dotNetProjectGuid;
        }

        public static bool IsNetCoreProjectJson(this Project project)
        {
            return Guid.TryParse(project?.Kind, out var projectGuid) && projectGuid == _dnxProjectGuid;
        }
    }
}