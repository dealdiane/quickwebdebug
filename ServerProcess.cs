﻿using Microsoft.Web.Administration;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Dealdiane.QuickWebDebug
{
    public class ServerProcess
    {
        public ServerProcess(Process process)
            : this(null, process)
        {
        }

        public ServerProcess(ApplicationDomain appDomain)
            : this(appDomain, Process.GetProcessById(appDomain.WorkerProcess.ProcessId))
        {
        }

        public ServerProcess(ApplicationDomain appDomain, Process process)
        {
            AppDomain = appDomain;
            Process = process;

            if (AppDomain == null)
            {
                Title = String.Format("[{0}] {1}.exe", Process.Id, Process.ProcessName);
            }
            else
            {
                Title = String.Format("[{0}][{1}] {2}.exe", Process.Id, AppDomain.Id, Process.ProcessName);
            }
        }

        public ApplicationDomain AppDomain { get; private set; }

        public Process Process { get; private set; }

        public string Title
        {
            get;
            private set;
        }
    }
}